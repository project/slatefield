<?php

declare(strict_types=1);

namespace Drupal\slatefield;

use Drupal\Component\Plugin\PluginManagerInterface;

interface SlatePluginManagerInterface extends PluginManagerInterface {

  /**
   * Get all available toolbar items.
   *
   * @return string[]
   *   The machine names of all available CKEditor 5 toolbar items.
   */
  public function getAllToolbarItems(): array;

}
