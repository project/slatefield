<?php

declare(strict_types=1);

namespace Drupal\slatefield;

use Drupal\Component\Annotation\Plugin\Discovery\AnnotationBridgeDecorator;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\YamlDiscoveryDecorator;
use Drupal\slatefield\Annotation\SlateComponent;
use Drupal\slatefield\Plugin\SlateComponentDefinition;
use Drupal\slatefield\Plugin\SlateComponentInterface;

final class SlatePluginManager extends DefaultPluginManager implements SlatePluginManagerInterface {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct("Plugin/slate", $namespaces, $module_handler, SlateComponentInterface::class, SlateComponent::class);
    $this->alterInfo('slate_component_info');
    $this->setCacheBackend($cache_backend, 'slate_component_plugins');
  }

  /**
   * {@inheritDoc}
   */
  protected function getDiscovery() {
    if (!$this->discovery) {
      $discovery = new AnnotatedClassDiscovery($this->subdir, $this->namespaces, $this->pluginDefinitionAnnotationName, $this->additionalAnnotationNamespaces);
      $discovery = new YamlDiscoveryDecorator($discovery, 'slate.components', $this->moduleHandler->getModuleDirectories());
      $discovery->addTranslatableProperty('label');
      $discovery = new AnnotationBridgeDecorator($discovery, $this->pluginDefinitionAnnotationName);
      $this->discovery = $discovery;
    }
    return $this->discovery;
  }

  /**
   * {@inheritDoc}
   */
  public function getAllToolbarItems(): array {
    return array_reduce($this->getDefinitions(), function (array $items, SlateComponentDefinition $component_definition) {
      return array_unique(array_merge($items, $component_definition->getToolbarItems()));
    }, []);
  }

}
