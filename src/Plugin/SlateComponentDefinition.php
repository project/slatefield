<?php

declare(strict_types=1);

namespace Drupal\slatefield\Plugin;

use Drupal\Component\Plugin\Definition\PluginDefinition;

class SlateComponentDefinition extends PluginDefinition {

  protected $library;

  protected $toolbarItems;

  /**
   * LayoutDefinition constructor.
   *
   * @param array $definition
   *   An array of values from the annotation.
   */
  public function __construct(array $definition) {
    [
      'library' => $library,
      'toolbar_items' => $toolbar_items,
    ] = $definition;
    $this->library = $library;
    $this->toolbarItems = $toolbar_items;
  }

  public function getLibrary(): string {
    return $this->library;
  }

  public function getToolbarItems(): array {
    return $this->toolbarItems ?: [];
  }

}
