<?php

namespace Drupal\slatefield\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\filter\Render\FilteredMarkup;

/**
 * Plugin implementation of the 'slate' formatter.
 *
 * @FieldFormatter(
 *   id = "slate",
 *   label = @Translation("Slate"),
 *   field_types = {
 *     "slate",
 *   },
 * )
 */
class SlateFormatter extends FormatterBase {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    return [
      '#markup' => FilteredMarkup::create($items->first()->value),
    ];
  }

}
