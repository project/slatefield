<?php

namespace Drupal\slatefield\Plugin\Field\FieldWidget;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slatefield\Plugin\SlateComponentDefinition;
use Drupal\slatefield\SlatePluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Slate field widget.
 *
 * @FieldWidget(
 *   id = "slate_widget_default",
 *   label = @Translation("Slate"),
 *   field_types = {
 *     "slate"
 *   }
 * )
 */
final class SlateWidget extends WidgetBase {

  /**
   * @var \Drupal\slatefield\SlatePluginManagerInterface
   */
  protected $slatePluginManager;

  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, SlatePluginManagerInterface $slate_plugin_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->slatePluginManager = $slate_plugin_manager;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $slate_plugin_manager = $container->get('plugin.manager.slate');
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings'], $slate_plugin_manager);
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $ckeditor_config = $this->getFieldSetting('ckeditor');
    $element_id = Html::getUniqueId('slate-textarea');
    $enabled_toolbar_items = Json::decode($ckeditor_config['components']);
    $libraries = array_reduce($this->slatePluginManager->getDefinitions(), function (array $libraries, SlateComponentDefinition $definition) use ($enabled_toolbar_items) {
      if (!empty(array_intersect($enabled_toolbar_items, $definition->getToolbarItems()))) {
        $library = $definition->getLibrary();
        $libraries[$library] = $library;
      }
      return $libraries;
    }, ['slatefield/attach-ckeditor' => 'slatefield/attach-ckeditor']);
    $element['value'] = [
      '#type' => 'textarea',
      '#default_value' => $items[$delta]->value,
      '#attributes' => [
        'id' => $element_id,
      ],
      '#attached' => [
        'library' => $libraries,
        'drupalSettings' => [
          'slate' => [
            'attachCkeditor' => [
              "textarea#$element_id" => $ckeditor_config,
            ],
          ],
        ],
      ],
    ];
    return $element;
  }

}
