<?php

namespace Drupal\slatefield\Plugin\Field\FieldType;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Random;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItemBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slatefield\Plugin\SlateComponentDefinition;
use Drupal\slatefield\SlatePluginManagerInterface;

/**
 * Defines the 'slate' field type.
 *
 * @FieldType(
 *   id = "slate",
 *   label = @Translation("Slate"),
 *   description = @Translation("A field containing HTML."),
 *   category = @Translation("General"),
 *   default_widget = "slate_widget_default",
 *   default_formatter = "slate",
 *   default_value = null,
 *   cardinality = 1,
 * )
 */
class SlateItem extends StringItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'blob',
          'size' => 'big',
        ],
      ],
    ];
  }

  public static function defaultFieldSettings() {
    return [
      'ckeditor' => [
        'components' => '[]',
      ],
    ];
  }

  public function getSlatePluginManager(): SlatePluginManagerInterface {
    if (!isset($this->slatePluginManager)) {
      $this->slatePluginManager = \Drupal::service('plugin.manager.slate');
    }
    return $this->slatePluginManager;
  }

  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = [
      '#type' => 'container',
    ];
    $ckeditor_config = $this->getSetting('ckeditor');
    $available = $this->getSlatePluginManager()->getAllToolbarItems();
    $form['ckeditor']['app'] = [
      '#type' => 'inline_template',
      '#template' => '<div id="ckeditor5-toolbar__app"></div><div id="ckeditor5-toolbar__buttons-available">{{ available }}</div>',
      '#context' => ['available' => Json::encode($available)],
      '#attached' => [
        'library' => ['slatefield/ckeditor5.admin'],
      ],
    ];
    $form['ckeditor']['components'] = [
      '#type' => 'textarea',
      '#title' => t('Capabilities'),
      '#default_value' => $ckeditor_config['components'],
      '#attributes' => [
        'id' => 'ckeditor5-toolbar__buttons-selected',
      ],
    ];
    return $form;
  }

  public function preSave() {
    $allowed_tags = ['h2', 'strong', 'i', 'p'];
    $this->value = Xss::filter($this->value, $allowed_tags);
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    return [['value' => sprintf("<p>%s</p>", $random->paragraphs(1))]];
  }

}
