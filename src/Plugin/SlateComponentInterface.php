<?php

declare(strict_types=1);

namespace Drupal\slatefield\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

interface SlateComponentInterface extends PluginInspectionInterface {}
