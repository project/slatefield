<?php

declare(strict_types=1);

namespace Drupal\slatefield\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\slatefield\Plugin\SlateComponentDefinition;

/**
 * Class SlateComponent
 *
 * @Annotation
 */
class SlateComponent extends Plugin {

  public $id;

  public $label;

  /**
   * {@inheritdoc}
   */
  public function get() {
    return new SlateComponentDefinition($this->definition);
  }

}
