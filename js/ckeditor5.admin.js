(function (Drupal) {

  const statements = {
    availableName(name) {
      return Drupal.t('Available button @name.', { '@name': name });
    },
    activeName(name) {
      return Drupal.t('Active button @name.', { '@name': name });
    },
    activeFirst: Drupal.t('This button is in the first position in the toolbar.'),
    activeLast: Drupal.t('This button is in the last position in the toolbar.'),
    add: Drupal.t('Press the down arrow key to add to the toolbar.'),
    remove: Drupal.t('Press the up arrow key to remove from the toolbar.'),
    sort: Drupal.t('Move this button in the toolbar by pressing the left or right arrow keys.'),
    sortFirst: Drupal.t('Move this button down the toolbar by pressing the right arrow key.'),
    sortLast: Drupal.t('Move this button up the toolbar by pressing the left arrow key.'),
  }
  const announcements = {
    onFocusDisabled(name) {
      Drupal.announce(`${statements.availableName(name)} ${statements.add}`, 'assertive');
    },
    onFocusActive(name) {
      Drupal.announce(`${statements.activeName(name)} ${statements.sort} ${statements.remove}`,'assertive');
    },
    onFocusActiveFirst(name) {
      Drupal.announce(`${statements.activeName(name)} ${statements.activeFirst} ${statements.sortFirst} ${statements.remove}`, 'assertive');
    },
    onFocusActiveLast(name) {
      Drupal.announce(`${statements.activeName(name)} ${statements.activeLast} ${statements.sortLast} ${statements.remove}`, 'assertive');
    },
  }

  /**
   * CKEditor 5 Admin provided in admin.js, exposes mountApp() and unmountApp().
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches admin app to edit the CKEditor 5 toolbar.
   * @prop {Drupal~behaviorDetach} detach
   *   Detaches admin app from the CKEditor 5 configuration form on 'unload'.
   */
  Drupal.behaviors.ckeditor5Admin = {
    attach(context) {
      const container = context.querySelector('#ckeditor5-toolbar__app');
      const available = context.querySelector('#ckeditor5-toolbar__buttons-available');
      const selected = context.querySelector('[class*="editor-settings-toolbar-items"]')
      if (container && window.CKEDITOR5_ADMIN) {
        // Attempting to mount the app multiple times can cause errors.
        if (!container.dataset.hasOwnProperty('vApp')) {
          [available, selected]
            .filter((el) => el)
            .forEach((el) => {
              el.classList.add('visually-hidden');
            });

          CKEDITOR5_ADMIN.mountApp({ announcements });
        }
      }
    },
    detach(context, settings, trigger) {
      const container = context.querySelector('#ckeditor5-toolbar__app');
      if (container && trigger === 'unload' && window.CKEDITOR5_ADMIN) {
        CKEDITOR5_ADMIN.unmountApp();
      }
    }
  }
})(Drupal);
