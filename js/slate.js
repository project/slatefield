(function (Drupal, drupalSettings) {
  Drupal.behaviors.slate = {
    attach: function (context, settings) {

      Object.entries(drupalSettings.slate.attachCkeditor).forEach(([key, value]) => {
        // "textarea#$element_id" => $ckeditor_config,
        const container = context.querySelector(key);
        if (container) {
          console.log(CKEditor5);
          const { editorClassic } = CKEditor5;
          const { ClassicEditor } = editorClassic;
          const { basicStyles, heading, paragraph, essentials } = CKEditor5;

          console.log(`attaching to ${key}`, value);
          const config = {
            extraPlugins: [essentials.Essentials, paragraph.Paragraph, heading.Heading, basicStyles.Bold, basicStyles.Italic],
            toolbar: {
              items: JSON.parse(value.components),
            },
          }
          ClassicEditor.create(container, config)
            .then((editor) => {
              // Save a reference to the initialized instance.


              // editor.model.document.on('change:data', () => {
              //   const callback = callbacks.get(element.id);
              //   if (callback) {
              //     // Marks the field as changed.
              //     // @see Drupal.editorAttach
              //     callback();
              //   }
              // });
              console.log('success');
            })
            .catch((error) => {
              console.error(error);
            });
        }
      })
      // $('input.myCustomBehavior', context).once('myCustomBehavior').addClass('processed');
    }
  }
})(Drupal, drupalSettings);
